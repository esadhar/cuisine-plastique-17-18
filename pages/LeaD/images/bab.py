# -*- coding:utf-8 -*-

liste_mots_file = open("motsFR.txt")

for t in range(1,4):
    txt_src_name = "babel" + str(t) + ".txt"
    #print(txt_src_name)
    txbabel_file = open(txt_src_name)
    txbabel_txt = txbabel_file.read()

    finded_words = {}
    cleaned_babel_txt = txbabel_txt

    if t == 2:
        #print(txbabel_txt)


        for line in liste_mots_file.readlines():
            word = line.strip()
            if(len(word)) > 2:
                c = txbabel_txt.count(word)
                if c > 0:
                    print("finded word : "+word)
                    finded_words[word] = c
                    cleaned_babel_txt = cleaned_babel_txt.replace(word, '')

    print(finded_words)
    print(cleaned_babel_txt)

    finded_words_txt = "\n".join(finded_words)

    finded_words_file = open('finded-'+str(t)+'.txt', 'w')
    finded_words_file.write(finded_words_txt)

    cleaned_babel_file = open('cleaned-'+str(t)+'.txt', 'w')
    cleaned_babel_file.write(cleaned_babel_txt)

    txbabel_file.close()
    finded_words_file.close()
    cleaned_babel_file.close()
