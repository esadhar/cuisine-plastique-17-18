# cuisine-graphique


## Introduction terminal

### ~ (home)
Lorsque vous ouvrez votre terminal, par default vous vous trouvez dans votre Home, simbolisé par ce signe ~

### ls (liste)

ls : __l__i __s__te   
Lister les contenus du dossier ou vous vous trouvez

```bash
ls
```

### cd (change directory)
`cd` (__c__hange __d__irectory)   
il faut vous déplacer dans vos dossiers pour aller là ou vous voulez travailler, pour cela on utilise la commande
```bash
cd dossier/dossier/dossier
```

## récupérer

Une fois que vous êtes au bon endroit dans vos dossiers, vous allez récupérer le dépos git de travail (repository en anglais)

```bash
git clone https://gitlab.com/esadhar/cuisine-plastique-17-18.git
```

cette commande crée la où vous êtes un dossier _cusine-plastique_ (attention à la faute de frappe dans le nom du dépo)

on se déplace maintenant dans le dossier créé

```bash
cd cusine-plastique
```

Il n'est pas nécessaire de faire `git init` car ce dossier est deja un dossier git

par contre on peut faire un `git status` pour voir ou en est le dépo

## Travailler

on liste les contenus de ce dossier

```bash
ls
```

on liste le contenu du dossier _page_

```bash
ls pages/
```

le dossier _pages_ contient un dossier _master_ qui est votre modele de départ

on duplique le dossier _master_ pour faire votre dossier de travail personel
pour cela on utilise la commande cp : copier (copy)

```bash
cp pages/master pages/mon_nom_mon_prenom
```

on ouvre le dossier avec atom ou un autre éditeur de code.

les deux fichiers avec les quels vous allez travailler sont _index.html_ est _styles.css_



## Add, Commit

L'enregistrement de votre travail avec git se fais en deux étapes, d'abords add puis commit

Je vérifie ce qui a changé

```bash
git status
```

j'ajoute (add) les changements à la liste

```bash
git add page/mon_nom_mon_prenom/index.html
git add page/mon_nom_mon_prenom/styles.css
```

j'enregistre (commit)

```bash
git commit -m "mon message de commit que je personnalise à chaque fois"
```

## Push, Pull


### récupérer (tirer) les contenu en ligne
```bash
git pull origin master
```
### envoyer (pousser) les contenus en ligne
```bash
git push origin master
```

## HTML / CSS

### liste de toutes les balises html disponibles
https://developer.mozilla.org/fr/docs/Web/HTML/Element

### liste de toutes les propriétés css disponibles
https://developer.mozilla.org/en-US/docs/Web/CSS/Reference

## générateur de couleurs
https://www.w3schools.com/colors/
