#!/usr/bin/python
# -*- coding: utf-8 -*-

# @Author: Bachir Soussi Chiadmi <bach>
# @Date:   18-05-2017
# @Email:  bachir@esadhar.net
# @Last modified by:   bach
# @Last modified time: 21-04-2017
# @License: GPL-V3


import sys, os, shutil
import markdown
# import mistune
from bs4 import BeautifulSoup
import pypandoc
import json
import re

_SRC_d = 'pages'
_BUILD_d = "build"
_IMG_path = os.path.join(_BUILD_d, 'images')
# CUR_PATH = os.path.dirname(os.path.abspath(__file__))

print("Building book")

def main():
   # clean build directory
   if os.path.isdir(_BUILD_d):
      shutil.rmtree(_BUILD_d, ignore_errors=True)
   os.mkdir(_BUILD_d)
   os.mkdir(_IMG_path)

   parse_pages()

def parse_pages():
   print("Parse book")

   # get template html
   template_f = open("templates/index.tpl.html", "r")
   template_html = template_f.read()
   template_dom = BeautifulSoup(template_html, 'html.parser')

   scss = "// compiled css\n\n"

   # loop through pages folders
   for page_d in os.listdir(_SRC_d):
      # avoid master folder
      if page_d == "master":
         continue
      print('\n')
      print('-- page : '+page_d)

      page_id = page_d.replace(' ', '-')

      page_d_path = os.path.join(_SRC_d,page_d)
      print("page_d_path : "+page_d_path)
      index_f_path = os.path.join(page_d_path, 'index.html')
      if not os.path.isfile(index_f_path):
         print("Index path is not a file, can't generate html : "+page_id+" | "+index_f_path)
         continue

      styles_f_path = os.path.join(page_d_path, 'styles.css')
      if not os.path.isfile(styles_f_path):
         print("Styles path is not a file, can't generate css : "+page_id+" | "+styles_f_path)
         continue

      page_f = open(index_f_path, "r")
      page_html = page_f.read()
      page_dom = BeautifulSoup(page_html, 'html.parser')

      img_index = 0

      styles_f = open(styles_f_path, "r")
      styles = styles_f.read()

      # TODO: detect images and copy them
      # background-image: url("balou/Cap1.jpg")
      # m = re.searchAll(r"background-image", styles)
      # m = re.search(r"background-image\d?:\d?url\(([^)]+)\)", styles)
      m = re.findall('background-image\s?:\s?url\(([^)]+)\)',styles,re.MULTILINE)
      if(len(m)):
         print('styles background-image')
         for bg_img in m:
            bg_img = re.sub('^"', '', bg_img)
            bg_img = re.sub('"$', '', bg_img)
            print('bg_img : '+bg_img)
            # print m.group(1)
            bg_img_path = os.path.join(page_d_path, bg_img)

            bg_img_name, bg_img_ext = os.path.splitext(bg_img)
            dest_bg_img_name = page_id+'-'+str(img_index)+bg_img_ext
            dest_bg_img_path = os.path.join(_IMG_path, dest_bg_img_name)
            if os.path.isfile(bg_img_path):
               shutil.copyfile(bg_img_path, dest_bg_img_path)
            else:
               print('!! bg img file not found !!')
               print('bg_img_path : '+bg_img_path)
               print('dest_bg_img_path : '+dest_bg_img_path)

            styles = styles.replace(bg_img, '../build/images/'+dest_bg_img_name)

            img_index = img_index+1

      scss += "."+page_id+"{\n"
      scss += "\t"+styles
      scss += "} " + "//end of " + page_id + "\n\n"

      # replace .paper by &.paper
      scss = re.sub('\.paper', '&', scss);

      # images_d_path = os.path.join(page_d_path, 'images')
      # if os.path.isdir(images_d_path):
      #    for img in os.listdir(images_d_path):
      #       shutil.copyfile(os.path.join(images_d_path,img), os.path.join(_IMG_path,img))
      # copy only used images and rename them

      for paper in page_dom.find_all('div', {"class":"paper"}):
         paper['class'].append(page_id)

         # insert headers and footers
         header = page_dom.new_tag('header')
         header['class'] = 'page-header'
         header.string = "2dg 2017-2018 : Recettes Graphiques"
         paper.find('div', {'class':"page"}).insert(0,header)

         footer = page_dom.new_tag('footer')
         footer['class'] = 'page-footer'
         footer.string = page_id
         paper.find('div', {'class':"page"}).append(footer)




         for img in paper.find_all('img'):
            # print('-- -- new image '+str(img_index))
            # print(img['src'])
            src = img['src']
            # print(src)

            img_name, img_ext = os.path.splitext(src)
            img_name = page_id+'-'+str(img_index)+img_ext
            # print(img_name)

            img_path = os.path.join(page_d_path, src)

            dest_img_path = os.path.join(_IMG_path, img_name)
            if os.path.isfile(img_path):
               shutil.copyfile(img_path, dest_img_path)
            else:
               print('!! img file not found !!')
               print('src : '+src)
               print('img_path : '+img_path)
               print('dest_img_path : '+dest_img_path)

            newsrc = '../build/images/'+img_name
            img['src'] = newsrc

            img_index = img_index+1


         # insert the page before 3em de couverture
         template_dom.find('div', {"id":"couve3"}).insert_before(paper)


   # create main html file from filled template html dom
   build_html_f = os.path.join(_BUILD_d,'index.html')
   with open(build_html_f, 'w') as fp:
      fp.write(template_dom.prettify())

   build_scss_f = os.path.join(_BUILD_d,'styles.scss')
   with open(build_scss_f, 'w') as fp:
      fp.write(scss)


if __name__ == "__main__":
   main()
